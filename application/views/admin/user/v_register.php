<!DOCTYPE html>
<html lang="en">
<head>
  
  <?php $this->load->view("admin/user/csslogres.php") ?>
  <title>Pendaftaran Pengguna</title>
</head>
<body id="page-top">

<div class="container">
 <div class="d-flex justify-content-center h-100">
  <div class="card">
   <div class="card-header">
   <center>
    <h3>Daftar Akun</h3>
  </center>
   </div>
   <?php
                  $error_msg=$this->session->flashdata('error_msg');
                  if($error_msg){
                    echo $error_msg;
                  }
                   ?>

   <div class="card-body">
    <form role="form" method="post" action="<?php echo base_url('admin/user/register_user'); ?>">
     <fieldset>
                              <div class="form-group">
                                  <input class="form-control" placeholder="Nama" name="user_name" type="text" autofocus>
                              </div>

                              <div class="form-group">
                                  <input class="form-control" placeholder="E-mail" name="user_email" type="email" autofocus>
                              </div>
                              <div class="form-group">
                                  <input class="form-control" placeholder="Password" name="user_password" type="password" value="">
                              </div>


                             <div class="form-group">
      <input type="submit" value="Daftar" class="btn float-right login_btn">
     </div>

                          </fieldset>
    </form>
   </div>
   <div class="card-footer">
    <div class="d-flex justify-content-center links">
    <center><b>Sudah Punya Akun ?</b> <br></b><a href="<?php echo base_url('admin/user/login_view'); ?>"><b>Masuk<b></a></center><!--for centered text-->
    </div>
   </div>
  </div>
 </div>
</div>


<?php $this->load->view("admin/_partials/scrolltop.php") ?>
<?php $this->load->view("admin/_partials/modal.php") ?>
<?php $this->load->view("admin/_partials/js.php") ?>
    
</body>
</html>
