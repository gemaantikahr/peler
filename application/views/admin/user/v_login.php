<!DOCTYPE html>
<html lang="en">
<head>
  
  <?php $this->load->view("admin/user/csslog.php") ?>
  <title>Login</title>
</head>
<body id="page-top">

<div class="container">
 <div class="d-flex justify-content-center h-100">
  <div class="card">
   <div class="card-header">
   <center>
    <h3>Masuk</h3>
  </center>
   </div>
   <?php
              $success_msg= $this->session->flashdata('success_msg');
              $error_msg= $this->session->flashdata('error_msg');

                  if($success_msg){
                    ?>
                    <div class="alert alert-success">
                      <?php echo $success_msg; ?>
                    </div>
                  <?php
                  }
                  if($error_msg){
                    ?>
                    <div class="alert alert-danger">
                      <?php echo $error_msg; ?>
                    </div>
                    <?php
                  }
                  ?>

   <div class="card-body">
    <form role="form" method="post" action="<?php echo base_url('admin/user/login_user'); ?>">
     <div >
       <font color="red"> <i class="fas fa-user"></i></font>
       <span><font color="white"> Email </font></span>
      </div>
     <div class="input-group form-group">
      <input type="text" class="form-control" placeholder="Email" name="user_email" required>     
     </div>
     <div >
       <font color="red"> <i class="fas fa-key"></i></font>
       <span><font color="white"> Password </font></span>
      </div>
     <div class="input-group form-group">
      <input type="password" class="form-control" placeholder="Password" name="user_password" required>
     </div>
     <!-- <div class="row align-items-center remember">
      <input type="checkbox">Remember Me
     </div> -->
     <div class="form-group">
      <input type="submit" value="Masuk" class="btn float-right login_btn">
     </div>
    </form>
   </div>
   <div class="card-footer" hidden>
    <div class="d-flex justify-content-center links">
     Belum Punya Akun?<a href="<?php echo base_url('admin/user'); ?>"><b>Daftar</b></a>
    </div>
   <!-- <div class="d-flex justify-content-center">
    <a href="#">Forgot your password?</a> -->
    </div>
   </div>
  </div>
 </div>
</div>



</body>
</html>
