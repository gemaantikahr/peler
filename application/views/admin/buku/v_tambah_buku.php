<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/buku1/') ?>"><i class="fas fa-arrow-left"></i> Back</a>
					</div>
					<div class="card-body">

						<form action="<?php echo base_url().'admin/buku1/simpan_buku'?>" method="post" enctype="multipart/form-data" >

							<div class="form-group">
								<label for="name">Name*</label>
								<input class="form-control" type="text" name="xnama" placeholder="nama Buku . ." />
                            </div>

							<div class="form-group">
								<label for="name">Kelas*</label>
								<div class="form-group">
                                        <select class="form-control" id="sel1" name="xkelas">
                                            <option value="1">1 / SD</option>
                                            <option value="2">2 / SD</option>
                                            <option value="3">3 / SD</option>
                                            <option value="4">4 / SD</option>
                                            <option value="5">5 / SD</option>
                                            <option value="6">6 / SD</option>
											<option value="7">7 / SMP</option>
                                            <option value="8">8 / SMP</option>
                                            <option value="9">9 / SMP</option>
                                            <option value="10">10 / SMA</option>
                                            <option value="11">11 / SMA</option>
                                            <option value="12">12 / SMA</option>  
                                        </select>
                                    </div> 
                            </div>

                            <div class="form-group">
								<label for="name">Harga*</label>
								<input class="form-control" type="number" name="xharga" placeholder="Harga . ."/>
							</div>

							<div class="form-group">
								<label for="name">Stok*</label>
								<input class="form-control" type="number" name="xstok" placeholder="Stok . ."/>
							</div>

							<div class="form-group">
								<label for="inputUserName" class="col-sm-4 control-label">Photo</label>
								<div class="form-control">
                                	<input type="file" name="filefoto"/>
                                </div>
							</div>

							<input class="btn btn-success" type="submit" name="btn" value="Save" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* Isi Semua Data
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php $this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->


		<?php $this->load->view("admin/_partials/scrolltop.php") ?>

		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

</html>
