<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">
	<div id="wrapper">
		<div id="content-wrapper">
			<div class="container-fluid">
				<!-- DataTables -->
				<div class="card mb-3">
					<div class="card-header">
					<center><h4>Transaksi</h4></center>
					</div>
                    <p><?php foreach ($namasekolah as $data):?>
						<center><strong><?php echo $data->sekolah?></strong>
					<?php endforeach;?>
                    <br> Tanggal : <?php echo $tanggalnota?></center>
                    </p>
					<div class="card-body">
						<div>
							<table border=1 width="100%" cellspacing="0">
								<thead>
									<tr style="text-align: center;">
										<th>Nomer*</th>
										<th>Nama Barang*</th>
                                        <th>Kelas*</th>
                                        <th>Harga*</th>
										<th>Banyak*</th>
										<th>Total Harga*</th>
									</tr>
								</thead>
								<tbody style="text-align: center;">
                                    <?php
                                    $no = 0; 
                                    foreach ($detailtransaksi as $data):
                                    $no++; ?>
									<tr>
										<td>
											<?php echo $no?>
										</td>
										<td>
											<?php echo $data->nama_buku ?>
										</td>
                                        <td>
                                            <?php echo "Kelas ".$data->kelas_buku?>
                                        </td>
                                        <td>
                                            <?php echo $data->harga_buku?>
                                        </td>
										<td>
											<?php echo $data->banyak_barang?>
										</td>
										<td>
											Rp. <?php echo $data->total_harga?>
										</td>
									</tr>
                                    <?php endforeach; ?>
                                    <?php 
                                        foreach ($totalharga as $total):
                                    ?>
                                    <tr>
                                        <th colspan='5'>TOTAL HARGA</th><th colspan=2>Rp. <?php echo $total->totalharga?></th>
                                    </tr>
                                        <?php endforeach;?>
								</tbody>
							</table>
						</div>
					</div>
                    
				</div>

			</div>

		
		</div>
	</div>
</body>

</html>
