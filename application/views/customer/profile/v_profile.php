<!DOCTYPE html>
<html lang="en">
<head>
<title>Penjualan Buku</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Little Closet template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/bootstrap-4.1.2/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/font-awesome-4.7.0/css/font-awesome.min.css'); ?> " >

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/owl.theme.default.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/animate.csss'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/main_styles.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/responsive.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style/style.css'); ?> ">


</head>
<body>

<!-- Menu -->


<div class="super_container">

	<!-- Header -->

	<?php if ($this->session->userdata('email_sekolah')==null){
			 $this->load->view("customer/partials/navbarlogin.php");
	}else{
		$this->load->view("customer/partials/navbar.php");
	}
	?>


	<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->
	

		<div class="products">
			<div class="container">
				<div class="box-cont-alt">
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<div class="section_title text-center" style="margin-top: 1.5em; 
	font-family: 'Overpass', sans-serif;">My Profile</div>

	<hr style="border: 1px solid #757575; opacity: 0.2;">
					</div>
				</div>
				<div class="row page_nav_row">
					<div class="col">
						<div class="page_nav">
							
						</div>
					</div>
				</div>

					
	<div class="prof-center"> 

	<img src="<?php echo base_url('assets/images/user.png');?> "/>

	<div class="text-prof jumbo">
		<h1> <?php echo $this->session->userdata('nama_sekolah');?> 
	</h1></div>  

	<div class="block-prof" style="
	margin: 0 auto; width: 100%; height: 100%; margin-bottom: 3em;">

<br>
	<?php foreach($profile as $data):?>

		<div class="prof">

			<div class="prof flex-lmnt">
		<div class="prof address"><img src = " <?php echo base_url('assets/images/pin.svg')?>"/>
			<span>  <?php echo $data->alamat_sekolah?> </span>  </div>

		</div>

		<div class="prof flex-box">

			<div class="prof flex-lmnt"> 
		<div class="prof phone">
			<img src = " <?php echo base_url('assets/images/phone.svg')?>"/>

			<h3> <?php echo $data->no_hp?> </h3> </div> </div>

			<div class="prof flex-lmnt"> 
		<div class="prof email">
			<img src = " <?php echo base_url('assets/images/email.svg')?>"/>
			<h3> <?php echo $data->email_sekolah?> </h3> </div>
			</div>

		</div>

		</div>


   
	<?php endforeach?>

	<hr style="border: 1px solid #757575; opacity: 0.2;">

	<a style="text-decoration: none;" href="<?php echo site_url ('customer/profile/edit_profile') ?>"> <button class="btn-prof">Edit Profile</button> </a>

					</div>
				</div>
			</div>

			</div>

		</div>

		<!-- Footer -->

	<?php $this->load->view("customer/partials/footer.php") ?>

		
	</div>
		
</div>

<script src="assets2/js/jquery-3.2.1.min.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/popper.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="assets2/plugins/greensock/TweenMax.min.js"></script>
<script src="assets2/plugins/greensock/TimelineMax.min.js"></script>
<script src="assets2/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="assets2/plugins/greensock/animation.gsap.min.js"></script>
<script src="assets2/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="assets2/plugins/easing/easing.js"></script>
<script src="assets2/plugins/parallax-js-master/parallax.min.js"></script>
<script src="assets2/js/cart.js"></script>

</body>
</html>