<!DOCTYPE html>
<html lang="en">
<head>
<title>Cart</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Penjualan Buku">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/bootstrap-4.1.2/bootstrap.min.css'); ?> ">
<link href="<?php echo base_url('assets2/plugins/font-awesome-4.7.0/css/font-awesome.min.css'); ?> " rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/cart.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/cart_responsive.css'); ?> ">

</head>

<body>


<div class="super_container">

	<!-- Header -->

	
	<?php $this->load->view("customer/partials/navbar.php") ?>


	<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->

		<div class="home">
			<div class="home_container d-flex flex-column align-items-center justify-content-end">
				<div class="home_content text-center">
					<div class="home_title">Shopping Cart's <?php echo $this->session->userdata('nama_customer');?></div>
					<div class="breadcrumbs d-flex flex-column align-items-center justify-content-center">
						<ul class="d-flex flex-row align-items-start justify-content-start text-center">
							<li><a href="#">Home</a></li>
							<li>Your Cart</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- Cart -->

		<div class="cart_section">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="cart_container">
							
							<!-- Cart Bar -->
							<div class="cart_bar">
								<ul class="cart_bar_list item_list d-flex flex-row align-items-center justify-content-end">
									<li class="mr-auto">Product</li>
									<li>Kelas</li>
									<li>Harga</li>
									<li>Jumlah</li>
									<li>Option</li>
								</ul>
							</div>

							<!-- Cart Items -->
							<div class="cart_items">
								<ul class="cart_items_list">

									<!-- Cart Item -->
									<form action="<?php echo base_url().'customer/cart/simpan_update_transaksi'?>" method="post" enctype="multipart/form-data" >	
									<?php $no=0; foreach($buku as $data):?>
										<?php foreach($tampung as $key):?>
											<?php if($data->id_buku == $key->id_buku): $no++?>
											
											<li class="cart_item item_list d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-end justify-content-start">
												<div class="product d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start mr-auto">
													<div><div class="product_number"><?php echo $no?></div></div>
													<div><div class="product_image"><img src="<?php echo base_url('assets/images/'.$data->foto_barang) ?>" alt=""></div></div>
													<div class="product_name_container">
													<div class="product_id" hidden><a href="product.html"><input type="text" name="xbarang[]" value="<?php echo $data->id_buku?>"></a></div>
													<div class="product_name"><a href="product.html"><?php echo $data->nama_buku?> </a></div>
														<div class="product_text"><?php echo " Kelas ".$data->kelas_buku?></div>
													</div>
												</div>
												<div class="product_color product_text"><span>Color: </span><?php echo " Kelas ".$data->kelas_buku?></div>
												<div class="product_price product_text"><span>Harga: </span><?php echo $data->harga_buku?></div>
												<div class="product_quantity_container">
												<td width="50">
												<input class="form-control col-sm-12" type="number" name="xbanyak[]" value="<?php echo $key->jumlah_buku?>"/>
												</td>
												</div>
												<div class="product_total product_text">
												<a href="<?php echo site_url('customer/cart/hapusbuku/'.$data->id_buku) ?>"
											 class="btn btn-small btn-danger"><i class="fas fa-edit"></i> Hapus</a>
												</div>
											</li>
											
											<?php endif?>
										<?php endforeach?>	
									<?php endforeach?>
									<input class="btn btn-success" type="submit" name="btn" value="Save" />        
									<div class="button button_continue trans_200"><a href="<?php echo site_url('customer')?>">Lanjutkan Belanja</a></div>
									                
                        			</form>
								</ul>
							</div>

							<!-- Cart Buttons -->
							<div class="cart_buttons d-flex flex-row align-items-start justify-content-start">
								<div class="cart_buttons_inner ml-sm-auto d-flex flex-row align-items-start justify-content-start flex-wrap">
									<div class="button button_clear trans_200"><a href="<?php echo site_url('customer/cart/lanjutan')?>">Lanjutkan</a></div>
									
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
<!-- Footer -->

		
	<?php $this->load->view("customer/partials/footer.php") ?>
	
	</div>
		
</div>

<script src="assets2/js/jquery-3.2.1.min.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/popper.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="assets2/plugins/greensock/TweenMax.min.js"></script>
<script src="assets2/plugins/greensock/TimelineMax.min.js"></script>
<script src="assets2/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="assets2/plugins/greensock/animation.gsap.min.js"></script>
<script src="assets2/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="assets2/plugins/easing/easing.js"></script>
<script src="assets2/plugins/parallax-js-master/parallax.min.js"></script>
<script src="assets2/js/cart.js"></script>
</body>
</html>