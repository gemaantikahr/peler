<!-- Boxes -->

		<div class="boxes">
				<div class="row">
					<div class="col-box">
						<div class="boxes_container d-flex flex-row align-items-start justify-content-between flex-wrap">
						<P>
						<div class="box-container"> <img class="img-position-box" width="25%" style="float: left;" src="<?php echo base_url('assets/images/HaedarNasir.jpg')?>">
   <div class="tulisan">
						<dd> &nbsp;&nbsp;&nbsp;Mata pelajaran Pendidikan Al-Islam, Kemuhammadiyahan dan Bahasa Arab (ISMUBA) pada sekolah dan madrasah Muhammadiyah merupakan ciri khusus dan keunggulan. Tujuan utama mata pelajaran yang luas tentang Islam dan Kemuhammadiyahan, kemampuan berbahasa Arab, serta memiliki karakter yang kuat melalui pembelajaran, keteladanan dan pembiasaan yang menggembirakan. Untuk menciptakan proses pembelajaran yang efektif dan bermutu, Majelis Pendidikan Dasar dan Menengah Pimpinan Pusat Muhammadiyah menerbitkan buku pelajaran pendidikan ISMUBA, sebagai buku pegangan siswa pada sekolah dan madrasah Muhammadiyah di seluruh Indonesia, mulai tahun pelajaran 2017/2018.</dd>
						 &nbsp;&nbsp;&nbsp;Pimpinan Pusat Muhammadiyah memberikan penghargaan yang tinggi atas penerbitan buku - buku ISMUBA (Al-Islam, Kemuhammadiyahan dan Bahasa Arab) oleh Majelis Pendidikan Dasar dan Menengah Pimpinan Pusat Muhammadiyah. Buku pelajaran merupakan bagian yang sangat penting untuk menciptakan pembelajaran yang efektif da bermutu, juga memiliki fungsi yang sangat penting dalam proses literasi dan membangun budaya membaca di kalangan siswa. Buku - buku ini menjadi rujukan sekaligus jendela ilmu yang mencerahkan dan mencerdaskan bagi siswa untuk tumbuh dan berkembang sebagai generasi berkemajuan.</div>
						</div></P>
						

						</div>
					</div>
				</div>
			</div>

		<!-- Features -->
		<div class="boxes1">
				<div class="row">
					<div class="col-box1">
						<div class="boxes_container1 d-flex flex-row align-items-start justify-content-between flex-wrap">

						</div>
					</div>
				</div>
			</div>