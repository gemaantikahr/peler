
<!DOCTYPE html>
<html>
<head>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

</head>
<body>
	

<header class="header">
		<div class="header_overlay"></div>
		<div class="header_content d-flex flex-row align-items-center justify-content-start">
			<div class="logo">
				<a href="<?php echo site_url('customer')?>">
					<div class="d-flex flex-row align-items-center justify-content-start">
						<div><img src="<?php echo site_url('assets2/images/logo_1.png')?>"></div>
						<div>Toko Buku ISMUBA</div>
					</div>
				</a>	
			</div>
			<div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
			<nav class="main_nav">
				<ul class="d-flex flex-row align-items-start justify-content-start">
					<li><a href="<?php echo site_url('customer/main/buku_sd')?>">SD</a></li>
					<li><a href="<?php echo site_url('customer/main/buku_smp')?>">SMP</a></li>
					<li><a href="<?php echo site_url('customer/main/buku_sma')?>">SMA</a></li>
				</ul>
			</nav>
			<div class="header_right d-flex flex-row align-items-center justify-content-start ml-auto">
				<!-- Search -->
				<div class="header_search">
					<form action="<?php echo base_url().'customer/main/caribukuku'?>" method="post" enctype="multipart/form-data" >
						<input type="text" class="search_input" name="xcari" placeholder="Search Item" required="required">
						<button class="menu_search_button" type="submit"><img src="<?php echo base_url('assets2/images/search.png');?>" /></button>
					</form>
				</div>
				
  <div class="dropdown">
    <button class="btn" type="button" data-toggle="dropdown" style="margin-right: 10px; background-color: transparent;">
				<div class="user" style="margin: 0 auto"><a href="#"><div><img src="<?php echo site_url('assets2/images/user.svg')?>"></div></a></div> </button>
		<ul class="dropdown-menu">
      <li><a href="<?php echo site_url('customer/login/login_view')?>">Log in</a></li>
      <li><a href="<?php echo site_url('customer/login')?>">Daftar</a></li>
    </ul>
</div>
				
			</div>
		</div>
	</header>

</body>
</html>
