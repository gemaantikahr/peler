<!DOCTYPE html>
<html lang="en">
<head>
  
  <?php $this->load->view("customer/login/csslog.php") ?>
  <title>Login</title>
</head>
<body id="page-top">

<div class="container">
 <div class="d-flex justify-content-center h-100">
  <div class="card">
   <div class="card-header">
   <center>
    <h3>Sign In</h3>
  </center>
   </div>
   <?php
              $success_msg= $this->session->flashdata('success_msg');
              $error_msg= $this->session->flashdata('error_msg');

                  if($success_msg){
                    ?>
                    <div class="alert alert-success">
                      <?php echo $success_msg; ?>
                    </div>
                  <?php
                  }
                  if($error_msg){
                    ?>
                    <div class="alert alert-danger">
                      <?php echo $error_msg; ?>
                    </div>
                    <?php
                  }
                  ?>

   <div class="card-body">
    <form role="form" method="post" action="<?php echo base_url('customer/login/login_user_customer'); ?>">
     <div >
       <font color="red"> <i class="fas fa-user"></i></font>
       <span><font color="white"> Username </font></span>
      </div>
     <div class="input-group form-group">
     <input class="form-control" placeholder="E-mail" name="user_email" type="email" autofocus required>     
     </div>
     <div >
       <font color="red"> <i class="fas fa-key"></i></font>
       <span><font color="white"> Password </font></span>
      </div>
     <div class="input-group form-group">
     <input class="form-control" placeholder="Password" name="user_password" type="password" required>
     </div>
     <div class="row align-items-center remember">
      <input type="checkbox">Remember Me
     </div>
     <div class="form-group">
      <input type="submit" value="Login" class="btn float-right login_btn">
     </div>
    </form>
   </div>
   <div class="card-footer">
    <div class="d-flex justify-content-center links">
     Don't have an account?<a href="<?php echo base_url('customer/login'); ?>"><b> Sign Up </b></a>
    </div>
   <div class="d-flex justify-content-center">
    <a href="#">Forgot your password?</a>
    </div>
   </div>
  </div>
 </div>
</div>


<?php $this->load->view("admin/_partials/scrolltop.php") ?>
<?php $this->load->view("admin/_partials/modal.php") ?>
<?php $this->load->view("admin/_partials/js.php") ?>
    
</body>
</html>
