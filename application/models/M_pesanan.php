
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_pesanan extends CI_Model{

    public function pesananmasuk(){
        $query = $this->db->query("SELECT *FROM tbl_pesanan GROUP BY id_pembeli");
        return $query->result();
    }

    public function detail_pesanan($idpembeli){
        $query = $this->db->query("SELECT tbl_pesanan.jumlah_buku, tbl_pesanan.id_buku, tbl_pesanan.total_harga, tbl_buku.nama_buku, tbl_buku.kelas_buku, tbl_buku.harga_buku
        FROM tbl_pesanan, tbl_buku WHERE tbl_buku.id_buku = tbl_pesanan.id_buku AND tbl_pesanan.id_pembeli ='$idpembeli'");
        return $query->result();
    }

    public function detail_pembeli($idpembeli){
        $query = $this->db->query("SELECT tbl_sekolah.nama_sekolah, tbl_sekolah.no_hp, tbl_sekolah.id_sekolah, tbl_sekolah.alamat_sekolah
        FROM tbl_pesanan, tbl_sekolah WHERE tbl_sekolah.id_sekolah=tbl_pesanan.id_pembeli AND tbl_sekolah.id_sekolah='$idpembeli' GROUP BY tbl_pesanan.id_pembeli");
        return $query->result();
    }

    public function total_harga($idpembeli){
        $query = $this->db->query("SELECT sum(total_harga) as total_harga
        FROM tbl_pesanan WHERE id_pembeli='$idpembeli'");
        return $query->result();
    }

    public function hapus_pesanan($idsekolah){
        $query = $this->db->query("DELETE FROM tbl_pesanan WHERE id_pembeli='$idsekolah'");
        return $query;
    }

}