<?php
class Transaksi extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_transaksi');
		$this->load->model('m_buku');
		$this->load->model('m_pesanan');
		$this->load->library('upload');
	}


	function index(){
		$data['tabletransaksi']=$this->m_transaksi->get_transaksi();
		$data['transaksi']=$this->m_transaksi->get_all_transaksi();
		$data['totalharganya'] = $this->m_transaksi->total_harganya();
        $this->load->view('admin/transaksi/v_tampil_transaksi', $data);
	}
	
	function add(){
		$data['sekolah'] = $this->m_transaksi->get_sekolah();
		$data['buku'] = $this->m_transaksi->get_buku();
		$this->load->view('admin/transaksi/v_tambah_transaksi', $data);
	}

	function simpan_transaksi(){
		$idsekolah = $this->input->post('xsekolah');
		$idbarang = $this->input->post('xbarang');
		$banyakbarang = $this->input->post('xbanyak');
		$jumlah = count($idbarang);
		$y =0;
		for($x=0;$x<$jumlah;$x++){
			while($banyakbarang[$y]==null){
			$y++;
			}
			$harga = $this->m_transaksi->ambil_harga($idbarang[$x]);
			$this->m_transaksi->simpan_transaksi($idsekolah, $idbarang[$x], $banyakbarang[$y], $harga);
			$this->m_transaksi->update_stok($idbarang[$x], $banyakbarang[$y]);
			$this->m_transaksi->update_laporan_stok($idbarang[$x], $banyakbarang[$y]);
			$y++;
		}
		redirect('admin/transaksi/kecil');
	}

	function delete_sekolah(){
		$id = $this->input->post('xid');
		$this->m_transaksi->delete_sekolah($id);
		redirect('admin/transaksi');
	}

	function kecil(){
		$data['tabletransaksi']=$this->m_transaksi->get_transaksi();
		$data['transaksi']=$this->m_transaksi->get_all_transaksi();
		$data['kecil']=$this->m_transaksi->transaksi_kecil();
        $this->load->view('admin/transaksi/v_transaksi_kecil', $data);
	}

	public function lanjutkan($where, $tanggal){
		$data['detailtransaksi']=$this->m_transaksi->detail_transaksi($where, $tanggal);
		$data['totalharga']=$this->m_transaksi->detail_harga($where, $tanggal);
		$data['namasekolah']=$this->m_transaksi->nama_sekolah($where);
		$data['tanggalnota'] = $tanggal;
		$data['idsekolahnota'] = $where;
        $this->load->view("admin/transaksi/v_transaksi_lanjut", $data);
	}

	public function cetak_nota($where, $tanggal){
		$data['detailtransaksi']=$this->m_transaksi->detail_transaksi($where, $tanggal);
		$data['totalharga']=$this->m_transaksi->detail_harga($where, $tanggal);
		$data['namasekolah']=$this->m_transaksi->nama_sekolah($where);
		$data['tanggalnota'] = $tanggal;
		$data['idsekolahnota'] = $where;
		$this->load->view("admin/transaksi/v_cetak_nota", $data);
	}
	
	function masuk(){
		$data['pesanan']=$this->m_pesanan->pesananmasuk();
        $this->load->view('admin/transaksi/v_transaksi_masuk', $data);
	}

	function masuk_detail($idpembeli){
		$data['detailpesanan']=$this->m_pesanan->detail_pesanan($idpembeli);
		$data['detailpembeli']=$this->m_pesanan->detail_pembeli($idpembeli);
		$data['totalharga']=$this->m_pesanan->total_harga($idpembeli);
        $this->load->view('admin/transaksi/v_transaksi_masuk_detail', $data);
	}

	function simpan_transaksi_pesanan(){
		$idsekolah = $this->input->post('xcustomer');
		$idbarang = $this->input->post('xbarang');
		$banyakbarang = $this->input->post('xbanyak');
		$totalharga = $this->input->post('xtotal');
		$jumlah = count($idbarang);
		for($x=0;$x<$jumlah;$x++){
			$this->m_transaksi->simpan_transaksi_pesanan($idsekolah, $idbarang[$x], $banyakbarang[$x], $totalharga[$x]);
			$this->m_transaksi->update_stok($idbarang[$x], $banyakbarang[$x]);
			$this->m_transaksi->update_laporan_stok($idbarang[$x], $banyakbarang[$x]);
		}
		$this->m_pesanan->hapus_pesanan($idsekolah);
		redirect('admin/transaksi/kecil');
	}
	
	public function edit($idtransaksi){
		$data['edit_transaksi']=$this->m_transaksi->edit_transaksi($idtransaksi);
		$this->load->view("admin/transaksi/v_edit_transaksi", $data);
	}

	public function update_transaksi(){
		$id = $this->input->post("xid");
		$banyak = $this->input->post("xbanyak");
		$this->m_transaksi->updatetransaksi($id, $banyak);
		redirect('admin/transaksi/edit/'.$id);
	}

	public function hapus_tranasksi($id_transaksi, $id_sekolah){
		$this->m_transaksi->hapustranasksi($id_transaksi, $id_sekolah);
		redirect('admin/transaksi');
	}

    
}