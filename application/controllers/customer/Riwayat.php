<?php 


class Riwayat extends CI_Controller{
        
    public function __construct(){ 
        parent::__construct();
        $this->load->model('m_sekolah');
        $this->load->model('m_customer');
        $this->load->model('m_transaksi');
        $this->load->model('m_cart');
        $this->load->helper('url');
    }
    public function index()
	{
        $data['riwayat'] = $this->m_customer->riwayat_customer();
        $this->load->view("customer/riwayat/v_tampil_riwayat", $data);
    }

    public function rincian($tanggal){
        $data['detailtransaksi']=$this->m_cart->detail_transaksi($tanggal);
		$data['totalharga']=$this->m_cart->detail_harga();
        $this->load->view("customer/riwayat/v_tampil_rincian", $data);
    }

}
