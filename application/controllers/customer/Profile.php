
<?php 

class Profile extends CI_Controller{

    public function __construct(){ 
            parent::__construct();
            $this->load->model('m_sekolah');
            $this->load->helper('url');
    }

	public function index(){
            $data['profile']=$this->m_sekolah->view_profile();
            $this->load->view('customer/profile/v_profile', $data);    
    }

    
	public function edit_profile(){
            $data['profile']=$this->m_sekolah->view_profile();
            $this->load->view('customer/profile/v_edit_profile', $data);    
        }

    public function update_profile(){
        $nama = $this->input->post("xnama");
        $no = $this->input->post("xno");
        $alamat = $this->input->post("xalamat");
        $email = $this->input->post("xemail");
        $password = md5($this->input->post("xpassword"));
        $this->m_sekolah->update_user($nama, $no, $alamat, $email, $password);
        redirect("customer/profile");
    }
}




