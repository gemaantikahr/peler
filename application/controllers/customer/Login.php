<?php

class Login extends CI_Controller {

    public function __construct(){

            parent::__construct();
                $this->load->helper('url');
                $this->load->model('M_user');
                $this->load->model('M_sekolah');
                $this->load->model('M_customer');
                $this->load->model('m_kabupaten');
                $this->load->library('Session');

    }

    public function index(){
        $data['kabupaten'] = $this->M_sekolah->get_all_kabupaten();
        $data["kabupaten"] = $this->m_kabupaten->getAll();
        $this->load->view("customer/login/v_register_customer", $data);
    }

    public function register_customer(){
            $nama_sekolah = $this->input->post('user_name');
            $alamat_customer = $this->input->post('xalamat');
            $email_sekolah = $this->input->post('user_email');
            $no_hp = $this->input->post('number_phone');
            $id_kabupaten = $this->input->post('xidkabupaten');
            $password_customer = md5($this->input->post('user_password'));
            // $user=array(
            // 'nama_sekolah'=>$this->input->post('user_name'),
            // 'email_sekolah'=>$this->input->post('user_email'),
            // 'no_hp'=>$this->input->post('number_phone'),
            // 'password_customer'=>md5($this->input->post('user_password')),
            //     );
                //print_r($user);
            // $email_check=$this->M_user->email_check($user['user_email']);
        $email_check=$this->M_customer->email_check($email_sekolah);
        if($email_check){
        // $this->M_user->register_user($user);

        $this->M_customer->register_customer($nama_sekolah,$alamat_customer, $email_sekolah, $no_hp, $id_kabupaten, $password_customer);
        // $this->M_sekolah->tambah_customer($nama_sekolah,$alamat_customer, $email_sekolah, $no_hp, $id_kabupaten, $password_customer);
        // $id_pelanggan=$this->M_sekolah->ambil_id($email_sekolah);
        // $this->M_customer->update_id($email_sekolah,$id_pelanggan);
        $this->session->set_flashdata('success_msg', 'Registered successfully.Now login to your account.');
        redirect('customer/login/login_view');

        }
        else{
        $this->session->set_flashdata('error_msg', 'Error occured,Try again.');
        redirect('customer/login');
        }   

    }

    public function login_view(){
        $this->load->view("customer/login/v_login_customer");

    }

    function login_user_customer(){
        $user_login=array(

        'email_sekolah'=>$this->input->post('user_email'),
        'password_customer'=>md5($this->input->post('user_password'))

            );

            $data=$this->M_customer->login_user_costomer($user_login['email_sekolah'],$user_login['password_customer']);
            if($data)
            {
                $this->session->set_userdata('id_sekolah',$data['id_sekolah']);
                $this->session->set_userdata('email_sekolah',$data['email_sekolah']);
                $this->session->set_userdata('nama_sekolah',$data['nama_sekolah']);
                // <?php echo $this->session->userdata('nama_sekolah'); buat echo doang biar inget
                redirect('customer');

            }
            else{
                $this->session->set_flashdata('error_msg', 'Error occured,Try again.');
                $this->load->view("customer/login/v_login_customer");

            }


    }

    function user_profile(){

        $this->load->view('admin/user/v_user_profile.php');
        }

    public function user_logout_hiya(){
        $this->session->sess_destroy();
        redirect('customer', 'refresh');
    }

}

?>
