<?php 


class Main extends CI_Controller{

	public function __construct() 
        { 
            parent::__construct();
            $this->load->model('m_buku');
            $this->load->helper('url'); 
        }
        
    public function index()
	{
        $data['bukuku']=$this->m_buku->getAllsatu();
        $data['buku'] = $this->m_buku->buku_terlaris();
        $this->load->view("customer/main", $data);
    }
    
    public function buku_sd(){
        $data['buku']=$this->m_buku->bukusd();
        $this->load->view("customer/kategori/v_buku_sd", $data);
    }

    public function buku_smp(){
        $data['buku']=$this->m_buku->bukusmp();
        $this->load->view("customer/kategori/v_buku_smp", $data);
    }
    public function buku_sma(){
        $data['buku']=$this->m_buku->bukusma();
        $this->load->view("customer/kategori/v_buku_sma", $data);
    }

    public function caribukuku(){
        $cari = $this->input->post("xcari");
        $data['caribuku']=$this->m_buku->cari_buku($cari);
        $this->load->view("customer/kategori/v_cari", $data);
    }
}

?>